module gitlab.com/gitlab-org/ci-cd/runner-tools/tlsctl

go 1.13

require (
	github.com/fullsailor/pkcs7 v0.0.0-20190404230743-d7302db945fa // indirect
	github.com/sirupsen/logrus v1.4.2
	github.com/urfave/cli v1.22.1
	github.com/zakjan/cert-chain-resolver v0.0.0-20180703112424-6076e1ded272 // indirect
	gitlab.com/gitlab-org/gitlab-runner v12.4.0+incompatible
)
