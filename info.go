package main

import (
	"crypto/x509"
	"encoding/pem"
	"errors"
	"fmt"
	"io/ioutil"
	"os"
	"strings"

	"github.com/urfave/cli"
)

func makeInfoCMD() cli.Command {
	return cli.Command{
		Name:  "info",
		Usage: "Print information about each cert in a chain.",
		Flags: []cli.Flag{
			cli.StringFlag{Name: "path"},
		},
		Action: infoAction,
	}
}

func infoAction(c *cli.Context) error {
	CAChainPath := c.String("path")
	if CAChainPath == "" {
		return errors.New("--path not defined")
	}

	chain, err := ioutil.ReadFile(CAChainPath)
	if err != nil {
		return fmt.Errorf("read CA Chain in %s: %w", CAChainPath, err)
	}

	for {
		var block *pem.Block
		block, chain = pem.Decode(chain)
		if block == nil {
			break
		}

		cert, err := x509.ParseCertificate(block.Bytes)
		if err != nil {
			return fmt.Errorf("parsing pem block: %v", err)
		}

		_, _ = fmt.Fprintln(os.Stdout, "---")
		_, _ = fmt.Fprintf(os.Stdout, "Serial: %x\n", cert.SerialNumber)
		_, _ = fmt.Fprintf(os.Stdout, "Subject: %v\n", cert.Subject)
		_, _ = fmt.Fprintf(os.Stdout, "Issuer:  %v\n", cert.Issuer)
		_, _ = fmt.Fprintf(os.Stdout, "Valid:   from %v to %v\n", cert.NotBefore, cert.NotAfter)
		_, _ = fmt.Fprintf(os.Stdout, "Issuer URLs: %v\n", cert.IssuingCertificateURL)

		_, _ = fmt.Fprintf(os.Stdout, "Version: %v\n", cert.Version)
		_, _ = fmt.Fprintf(os.Stdout, "Key usage: %v\n", cert.KeyUsage)
		_, _ = fmt.Fprintf(os.Stdout, "Extended key usage: %v\n", cert.ExtKeyUsage)
		_, _ = fmt.Fprintf(os.Stdout, "DNS names:\n - %s\n", strings.Join(cert.DNSNames, "\n - "))
		_, _ = fmt.Fprintf(os.Stdout, "Public key algorithm: %v\n", cert.PublicKeyAlgorithm)

		_, _ = fmt.Fprintf(os.Stdout, "Is CA: %v\n", cert.IsCA)
		_, _ = fmt.Fprintf(os.Stdout, "SelfSigned: %v\n", cert.CheckSignatureFrom(cert) == nil)

		if chain == nil || len(chain) < 1 {
			break
		}
	}

	return nil
}
