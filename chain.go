package main

import (
	"crypto/x509"
	"encoding/pem"
	"errors"
	"fmt"
	"io/ioutil"
	"os"

	"github.com/urfave/cli"
)

type ChainElement struct {
	*x509.Certificate
	signedBy *ChainElement
}

func (ce *ChainElement) String() string {
	signer := "."
	if ce.signedBy != nil {
		signer = ce.signedBy.String()
	}

	name := ce.Subject.CommonName
	if ce.Certificate.CheckSignatureFrom(ce.Certificate) == nil {
		return fmt.Sprintf("%s (ROOT)", name)
	}

	return fmt.Sprintf("%s -> %s", name, signer)
}

func makeChainCMD() cli.Command {
	return cli.Command{
		Name:  "chain",
		Usage: "Print the chain of a certificate.",
		Flags: []cli.Flag{
			cli.StringFlag{Name: "path"},
		},
		Action: chainAction,
	}
}

func chainAction(c *cli.Context) error {
	caChainPath := c.String("path")
	if caChainPath == "" {
		return errors.New("--path is not defined")
	}

	data, err := ioutil.ReadFile(caChainPath)
	if err != nil {
		return fmt.Errorf("reading ca chain %q: %w", caChainPath, err)
	}

	var root *x509.Certificate
	certs := make([]*x509.Certificate, 0)

	for {
		var block *pem.Block
		block, data = pem.Decode(data)
		if block == nil {
			break
		}

		cert, err := x509.ParseCertificate(block.Bytes)
		if err != nil {
			return fmt.Errorf("parse certificate: %w", err)
		}

		if cert.CheckSignatureFrom(cert) == nil {
			root = cert
		} else {
			certs = append(certs, cert)
		}

		if data == nil || len(data) < 1 {
			break
		}
	}

	// If no root was found, take the last one from the chain.
	if root == nil {
		if len(certs) <= 1 {
			return errors.New("no root certificate found")
		}

		root = certs[len(certs)-1]
		certs = certs[0:(len(certs) - 1)]
	}

	chainHead := &ChainElement{Certificate: root}

	for {
		found := false

		for _, cert := range certs {
			if cert.CheckSignatureFrom(chainHead.Certificate) == nil {
				element := &ChainElement{
					Certificate: cert,
					signedBy:    chainHead,
				}
				chainHead = element
				found = true
			}
		}

		if !found {
			break
		}
	}

	_, err = fmt.Fprint(os.Stdout, chainHead.String())
	if err != nil {
		return fmt.Errorf("pringing output: %w", err)
	}

	return nil
}
