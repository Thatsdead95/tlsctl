package main

import (
	"crypto/x509"
	"encoding/pem"
	"errors"
	"fmt"
	"io/ioutil"
	"net/http"
	"net/url"
	"os"
	"path/filepath"

	"github.com/sirupsen/logrus"
	"github.com/urfave/cli"
	"gitlab.com/gitlab-org/gitlab-runner/helpers/tls/ca_chain"
)

var (
	baseDir = filepath.Join(".", "data")

	outDir string
)

func makeSaveCMD() cli.Command {
	return cli.Command{
		Name:  "save",
		Usage: "Use GitLab Runner TLS verification and save the chain in a file",
		Flags: []cli.Flag{
			cli.StringFlag{Name: "url"},
		},
		Action: saveAction,
	}
}

func saveAction(c *cli.Context) error {
	URL := c.String("url")
	if URL == "" {
		return errors.New("--url not defined")
	}

	err := prepareOutputDirectory(URL)
	if err != nil {
		return fmt.Errorf("preparing output directory: %w", err)
	}

	response, err := http.Head(URL)
	if err != nil {
		return fmt.Errorf("sending request for TLS: %w", err)
	}
	if response.TLS == nil {
		return errors.New("no TLS response found")
	}

	b := ca_chain.NewBuilder()
	err = b.FetchCertificatesFromTLSConnectionState(response.TLS)
	if err != nil {
		return fmt.Errorf("fetching certificates from TLS: %w", err)
	}

	chain := []byte(b.String())

	err = saveCAChain(chain)
	if err != nil {
		return fmt.Errorf("saving chain to file: %v", err)
	}

	for {
		var block *pem.Block
		block, chain = pem.Decode(chain)

		err = savePemBlock(block)
		if err != nil {
			return fmt.Errorf("saving pem block to file: %w", err)
		}

		if chain == nil || len(chain) < 1 {
			break
		}
	}

	return nil
}

func prepareOutputDirectory(URL string) error {
	u, err := url.Parse(URL)
	if err != nil {
		return fmt.Errorf("parsing URL %q: %w", URL, err)
	}

	outDir = filepath.Join(baseDir, u.Hostname())

	err = mkdirIfNotExists(outDir)
	if err != nil {
		return fmt.Errorf("creating output directory %q: %w", outDir, err)
	}

	return nil
}

func mkdirIfNotExists(path string) error {
	_, err := os.Stat(path)
	if os.IsNotExist(err) {
		err := os.MkdirAll(path, 0700)
		if err != nil {
			return err
		}
	}

	return nil
}

func savePemBlock(block *pem.Block) error {
	cert, err := x509.ParseCertificate(block.Bytes)
	if err != nil {
		return fmt.Errorf("parsing certificate: %w", err)
	}

	filePath := filepath.Join(outDir, cert.SerialNumber.String())

	f, err := os.Create(filePath)
	if err != nil {
		return fmt.Errorf("creating file for cert %s: %w", cert.SerialNumber, err)
	}

	err = pem.Encode(f, block)
	if err != nil {
		return fmt.Errorf("writing cert to file %s: %w", f.Name(), err)
	}

	logrus.WithFields(logrus.Fields{
		"serial":  cert.SerialNumber,
		"subject": cert.Subject,
		"path":    filePath,
	}).Info("Save cert file")

	return nil
}

func saveCAChain(chain []byte) error {
	path := filepath.Join(outDir, "CAChain.crt")

	logrus.WithField("path", path).Info("Save CAChain")

	return ioutil.WriteFile(path, chain, 0600)
}
