# tlsctl

A set of command line tools for to debug TLS certificates.

## Example

### Get CA from website

```shell
go build && ./tlsctl save --url https://example.com

INFO[0001] Save CAChain                                  path=/tmp/tlsctl/CAChain.crt
INFO[0001] Save cert file                                path=/tmp/tlsctl/21020869104500376438182461249190639870 serial=21020869104500376438182461249190639870 subject="CN=www.example.org,OU=Technology,O=Internet Corporation for Assigned Names and Numbers,L=Los Angeles,ST=California,C=US"
INFO[0001] Save cert file                                path=/tmp/tlsctl/2646203786665923649276728595390119057 serial=2646203786665923649276728595390119057 subject="CN=DigiCert SHA2 Secure Server CA,O=DigiCert Inc,C=US"
INFO[0001] Save cert file                                path=/tmp/tlsctl/10944719598952040374951832963794454346 serial=10944719598952040374951832963794454346 subject="CN=DigiCert Global Root CA,OU=www.digicert.com,O=DigiCert Inc,C=US"
```

### Get Info about CA chain

```shell
go build && ./tlsctl info --path /tmp/tlsctl/CaChain.crt

---
Serial: fd078dd48f1a2bd4d0f2ba96b6038fe
Subject: CN=www.example.org,OU=Technology,O=Internet Corporation for Assigned Names and Numbers,L=Los Angeles,ST=California,C=US
Issuer:  CN=DigiCert SHA2 Secure Server CA,O=DigiCert Inc,C=US
Valid:   from 2018-11-28 00:00:00 +0000 UTC to 2020-12-02 12:00:00 +0000 UTC
Issuer URLs: [http://cacerts.digicert.com/DigiCertSHA2SecureServerCA.crt]
SelfSigned: false
---
Serial: 1fda3eb6eca75c888438b724bcfbc91
Subject: CN=DigiCert SHA2 Secure Server CA,O=DigiCert Inc,C=US
Issuer:  CN=DigiCert Global Root CA,OU=www.digicert.com,O=DigiCert Inc,C=US
Valid:   from 2013-03-08 12:00:00 +0000 UTC to 2023-03-08 12:00:00 +0000 UTC
Issuer URLs: []
SelfSigned: false
---
Serial: 83be056904246b1a1756ac95991c74a
Subject: CN=DigiCert Global Root CA,OU=www.digicert.com,O=DigiCert Inc,C=US
Issuer:  CN=DigiCert Global Root CA,OU=www.digicert.com,O=DigiCert Inc,C=US
Valid:   from 2006-11-10 00:00:00 +0000 UTC to 2031-11-10 00:00:00 +0000 UTC
Issuer URLs: []
SelfSigned: true
```

### Get resolved chained

```shell
go build && ./tlsctl chain --path /tmp/tlsctl/CaChain.crt

www.example.org -> DigiCert SHA2 Secure Server CA -> DigiCert Global Root CA (ROOT)
```
